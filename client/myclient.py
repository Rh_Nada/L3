import http.client

target_hostname = "localhost"
target_port = 3000
default_timeout = 10

conn = http.client.HTTPConnection(
    host=target_hostname,
    port=target_port,
    timeout=default_timeout
    )

headers = { 'user-agent': "vscode-restclient" }

conn.request("GET", "/", headers=headers)

res = conn.getresponse()
data = res.read()
print(data.decode("utf-8"))


