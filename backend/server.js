const express = require('express')
const app = express()
const port = 3000;
const converter = require('./converter')
/**
 * This is our first endpoint"/"
 */

app.get('/', (req, res) => {
  res.send('Got your message');
})
app.get('/querying-something', (req, res) => {
  console.log('We can do something here server-side')
  const a = req.query.a;
  const b = req.query.b;
  console.log('Sever got: ${a} and ${b}');
  res.send(`Hey client , you sent queryparam a:"${a} and b:"${b}`);

})
app.get('/data', (req, res) => {
  res.send(`Here is your data:123`);
})
app.get('/name-query', (req, res) => {
  const fname = req.query.fname ;
  const lname = req.query.lname ;
  console.log(`Git the data for name-query`);
  res.send(`Your name is ${fname} ${lname}`);
})
app.get('/rgb-to-hex', (req, res) => {
  const r = req.query.red;
  const gr = req.query.green;
  const blu = req.query.blue;
  console.log({ r, gr, blu });
  const Hex = converter.ConvertRGBtoHex(parseInt(r),parseInt(gr),parseInt(blu));
  res.send(`HEX value: ${Hex}`)
})
app.use(express.json());
app.post('/add', (req, res) => {
  const client_name = req.body.client_name;
  const sum = req.body.num1 + req.body.num2;
  console.log(sum);
  res.send(`Clients: ${client_name} sum is ${sum}`);
})
app.get('/hex-to-rgb', (req, res) => {
  const Hex = req.query.Hex ;
  const rgb = converter.hexToRgb("#eb5643");
  console.log(rgb);
  res.send(rgb)
})
app.listen(port, () => {
  console.log(`Listening on http://localhost:${port}`);
})
